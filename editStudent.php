<?php
	include_once('inc/header.php');
	include_once('lib/Database.php');
	$db = new Database;
 ?>
<!-- Main Content Start here-->
<div class="panel panel-default">
	<div class="panel-heading">
		<h3>Update Student Info.<a class="btn btn-success pull-right" href="index.php">Students list</a></h3>
	</div>
	<?php 
		$id = $_GET['id'];
		$table = "tbl_student";
		$whereCon = array(
			'where'=>array('id'=> $id),
			'return_type'=>'single'
			);
		$value = $db->select($table, $whereCon);
		if (!empty($value)) { ?>
			
	<div class="panel body">
		<form action="lib/studentProcess.php" method="POST">
			<div class="form-group">
			<label for="name">Student name:</label>
				<input class="form-control" type="text" value="<?php echo $value['name']; ?>" name="name" id="name">
			</div>			
			<div class="form-group">
			<label for="email">Student email:</label>
				<input class="form-control" type="email" value="<?php echo $value['email']; ?>" name="email" id="email">
			</div>			
			<div class="form-group">
			<label for="phone">Phone:</label>
				<input class="form-control" type="phone" value="<?php echo $value['phone']; ?>" name="phone" id="phone">
			</div>			
			<div class="form-group">
				<input type="hidden" name="id" value="<?php echo $value['id']; ?>">
				<input type="hidden" name="action" value="edit">
				<input class="btn btn-primary" type="submit" value="Update Student" name="submit" id="add">
			</div>
		</form>
		
	</div>
	<?php } ?>
</div>




			<!-- Main Content End here-->


<?php
	include_once('inc/footer.php'); 
?>