<?php
	include_once('inc/header.php');
	include_once('lib/Database.php');
	include_once('lib/session.php');

	$db = new Database ();

	Session::init();
	$msg = Session::get('msg');
	if (!empty($msg)) {
		echo "<h3 class='alert-info text-center'>".$msg."</h3>";
		//Session::unsetSession();
		Session::set('msg',NULL); //Its are best in complex situation for unset message
	}

 ?>
<!-- Main Content Start here-->
<div class="panel panel-default">
	<div class="panel-heading">
		<h3>Student List <a class="btn btn-success pull-right" href="addStudent.php">Add Student</a></h3>
	</div>
	<table class="table table-striped">
		<tr>
			<th>Serial</th>
			<th>Name</th>
			<th>Email</th>
			<th>Phone</th>
			<th>Action</th>
		</tr>
		<?php 
			$table 		= "tbl_student";
			$order_by   = array('order_by' => 'id DESC');

		/* $selectcondition = array('select' => 'name');
			$wherecondition  = array(
				'where'		  =>array('id'=>'1', 'email'=>'karim@gmail.com'),
				'return_type' =>'single'
				);
			$limit = array('start'=>'2', 'limit'=> '4');
			$limit = array('limit'=> '3'); 
		*/

			$studentData = $db->select($table,$order_by);

			if (!empty($studentData)) {
				$i = 0;
				foreach ($studentData as $data) { $i++; ?>
				<tr>
					<td><?php echo $i; ?></td>
					<td><?php echo $data['name']; ?></td>
					<td><?php echo $data['email']; ?></td>
					<td><?php echo $data['phone']; ?></td>
					<td>
						<a class="btn btn-success" href="editStudent.php?id=<?php echo $data['id']; ?>">Edit</a>
						<a class="btn btn-danger" href="lib/studentProcess.php?action=delete&id=<?php echo $data['id']; ?>" onclick="return confirm('Are you sure to delete data?');" >Delete</a>
					</td>
				</tr>						
			<?php	} } else{ ?>
				<tr><td><h2> Data no found!</h2></td></tr>
			<?php	} ?>
				
		
	</table>
	
</div>

			<!-- Main Content End here-->


<?php
	include_once('inc/footer.php'); 
?>